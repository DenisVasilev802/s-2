class QueueException {
    private:
        char* message;
    public:
        QueueException(char* message);
        char* getMessage();
};
