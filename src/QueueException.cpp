#include "../include/QueueException.hpp"

QueueException::QueueException(char* message) {
    this->message = message;
}

char* QueueException::getMessage() {
    return message;
}
