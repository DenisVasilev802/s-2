#include "../include/queue.h"
#include "../include/QueueException.hpp"
#include <iostream>
Queue::Queue() {
    length = 0;
    first = 0;
    quantity = 0;
    last = 0;
    arr = new int[0];
}

Queue::Queue(int n) {
    length = n;
    quantity = 0;
    first = 0;
    last = 0;
    arr = new int[n];
}

Queue::Queue(const Queue& q) {
    length = q.length;
    quantity = q.quantity;
    arr = new int[length];
    first = q.first;
    last = q.last;
    if(first < last) {
        for(int i = first; i <= last; i++) {
            arr[i] = q.arr[i];
        }
    } else if(first > last) {
        for(int i = first; i < length; i++) {
            arr[i] = q.arr[i];
        }
        for(int i = 0; i <= last; i++) {
            arr[i] = q.arr[i];
        }
    }
}

Queue::~Queue() {
    length = 0;
    quantity = 0;
    first = 0;
    last = 0;
    delete[] arr;
}

void Queue::add(int elem) {

    if(quantity == length) {
        throw new QueueException("Add Error!");
    } else {
        quantity++;
        if(quantity != 1) last++;
        last %= length;
        arr[last] = elem;
    }
}

int Queue::getLength() {
    return length;
}

int Queue::getQuantity() {
    return quantity;
}

bool Queue::isEmpty() {
    return quantity == 0;
}

void Queue::makeEmpty() {
    if(first < last) {
        for(int i = first; i <= last; i++) {
            arr[i] = 0;
        }
    } else if(first > last) {
        for(int i = first; i < length; i++) {
            arr[i] = 0;
        }
        for(int i = 0; i <= last; i++) {
            arr[i] = 0;
        }
    }
    first = 0;
    last = 0;
    quantity = 0;
}

int Queue::peek() {
    if(quantity == 0) {
        throw new QueueException("Peek Error!");
    } else {
        return arr[first];
    }
}

int Queue::pop() {
    if(quantity == 0) {
        throw new QueueException("Pop Error!");
    } else {
        int result = arr[first];
        first++;
        first %= length;
        quantity--;
        return result;
    }
}

int Queue::getFirst() const {
    return first;
}

void Queue::setFirst(int first) {
    Queue::first = first;
}

int Queue::getLast() const {
    return last;
}

void Queue::setLast(int last) {
    Queue::last = last;
}

void Queue::setQuantity(int quantity) {
    Queue::quantity = quantity;
}

int *Queue::getArr() const {
    return arr;
}

void Queue::setArr(int *arr) {
    Queue::arr = arr;
}

void Queue::setLength(int length) {
    Queue::length = length;
}
