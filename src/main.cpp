#include <iostream>
#include "../include/queue.h"
#include "../include/QueueException.hpp"
#include "../include/iterators.hpp"

using namespace std;

int main()
{
    Queue q1();
    Queue q2(4);
    Queue q3(q2);
    q3.add(1);

    q3.add(2);

    q3.add(3);
    q3.add(4);

    try {
        q3.add(5);
    } catch(QueueException* e) {
        cout << e->getMessage() << endl;
    }
    cout << q3.pop() << endl;
    cout << "Quantity of elements of q3: " << q3.getQuantity() << endl;
    cout << "Length of q3: " << q3.getLength() << endl;
    cout << q3.pop() << endl;
    cout << q3.pop() << endl;
    cout << q3.pop() << endl;
    try {
        q3.pop();
    } catch(QueueException* e) {
        cout << e->getMessage() << endl;
    }
    q3.add(5);
    q3.add(6);
    q3.add(7);
    q3.add(8);
    q3.pop();
    q3.add(9);
    cout << q3.peek() << endl;
    cout << "Is q3 empty? ";
    q3.isEmpty()? cout << "true" << endl : cout << "false" << endl;
    q3.makeEmpty();
    cout << "Is q3 empty? ";
    q3.isEmpty()? cout << "true" << endl : cout << "false" << endl;

    Queue q{10};
    for(int i = 0; i < 10; i++) {
        q.add(i);
    }

    Iterators iterator(q);
    cout << iterator.getValue() << "+ ";
    iterator.next();
    iterator.next();
    //it.start();
    iterator.next();
    while(!iterator.finish()) {
        cout << iterator.getValue() << " ";
        iterator.next();
    }
    cout << iterator.getValue() << " ";
    cout << endl;
    iterator.start();
    while(!iterator.finish()) {
        cout << iterator.getValue() << " ";
        iterator.next();
    }
    cout << iterator.getValue() << " ";
    return 0;
}
