#include "../include/iterators.hpp"

Iterators::Iterators(Queue &que) {
	this->index = que.getFirst();
	this->queue = &que;
}

void Iterators::start() {
	index = queue->getFirst();
}

void Iterators::next() {
	index++;
	index = index % queue->getQuantity();
}

bool Iterators::finish() {
	if (queue->getLast() != index && !queue->isEmpty()) {
		return false;
	}
	return true;
}

int Iterators::getValue() {
	return queue->getArr()[index];
}


