

#ifndef _QUEUE_
#define _QUEUE_


class Queue {
private:
    int *arr;
    int first;
    int last;
    int quantity;
    int length;
public:
    Queue();

    int *getArr() const;

    void setArr(int *arr);

    void setLength(int length);

    void setQuantity(int quantity);

    Queue(int n);

    Queue(const Queue &q);

    ~Queue();

    void add(int elem);

    int pop();

    int peek();

    int getLength();

    int getQuantity();

    void makeEmpty();

    bool isEmpty();

    int getFirst() const;

    void setFirst(int first);

    int getLast() const;

    void setLast(int last);

};

#endif //_QUEUE_
