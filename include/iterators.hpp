#ifndef _ITERATOR_
#define _ITERATOR_


#include "queue.h"

class Iterators {
    Queue *queue;
private:

    int index;
public:
 Iterators(Queue &queue);

    void start();

    void next();

    bool finish();

    int getValue();



};

#endif
